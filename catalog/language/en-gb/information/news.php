<?php
// Text
$_['latest_news']           = 'Latest News';
$_['all_news']              = 'All News';
$_['see_more']              = 'See more';
$_['total_news']            = 'Total News:';
$_['heading_title']         = 'News';
$_['text_title']            = 'News Title';
$_['text_description']      = 'News Discription';
$_['button_all_news']       = 'All News';
?>