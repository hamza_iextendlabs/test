<?php
class ControllerExtensionModuleNews extends Controller {
	public function index() {
		$this->load->language('extension/module/news');

		$this->load->model('extension/module/news');
		$allNews = $this->model_extension_module_news->getNewss();

		$data['allNews'] = array();

		foreach($allNews as $singleNews){
			$data['allNews'][] = array(
				'title' => $singleNews['title'],
				'href'  => $this->url->link('extension/module/news/view', 'news_id=' . $singleNews['news_id']),
				'date_time' =>$singleNews['date_time']
			);
		}

		$latestNews = $this->model_extension_module_news->getLatestNews();

		$data['latestNews'] = array();

		foreach($latestNews as $news){
			$data['latestNews'][] = array(
				'title' => $news['title'],
				'href'  => $this->url->link('extension/module/news/view', 'news_id=' . $news['news_id']),
				'date_time' =>$news['date_time']
			);
		}

		

		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['href'] = 
		$this->response->setOutput($this->load->view('extension/module/news', $data));
	}

	public function allNews() {
		$this->load->language('extension/module/news');

		$this->load->model('extension/module/news');
		$allNews = $this->model_extension_module_news->getAllNews();

		$data['allNews'] = array();

		foreach($allNews as $singleNews){
			$data['allNews'][] = array(
				'title' => $singleNews['title'],
				'href'  => $this->url->link('extension/module/news/view', 'news_id=' . $singleNews['news_id']),
				'date_time' =>$singleNews['date_time']
			);
		}
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('information/allnews', $data));
	}

	public function view() {

        $this->load->language('extension/module/news');
        $this->load->model('extension/module/news');


		if (isset($this->request->get['news_id'])) {
			$news_id = (int)$this->request->get['news_id'];
		} else {
			$news_id = 0;
		}

        $news_info = $this->model_extension_module_news->getNews($news_id);
        
        if($news_info){
            $data['title'] = $news_info['title'];
			$data['description'] = html_entity_decode($news_info['description']);

            $data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
            $data['allnews'] = $this->url->link('extension/module/news');
            
            $this->response->setOutput($this->load->view('information/view', $data));
        }

            
    }
}
