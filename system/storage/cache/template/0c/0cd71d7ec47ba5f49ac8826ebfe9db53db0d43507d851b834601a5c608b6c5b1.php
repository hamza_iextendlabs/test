<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/test/test.twig */
class __TwigTemplate_4b000fea0f056b073e0f7257db807a401c4cc182c988a66176ba099370aae626 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        echo ($context["entry_status"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"test_status\" id=\"input-status\" class=\"form-control\">
                ";
        // line 4
        if (($context["test_status"] ?? null)) {
            // line 5
            echo "                <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\">";
            // line 6
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        } else {
            // line 8
            echo "                <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 9
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        }
        // line 11
        echo "              </select>
";
    }

    public function getTemplateName()
    {
        return "default/template/test/test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 11,  61 => 9,  56 => 8,  51 => 6,  46 => 5,  44 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/test/test.twig", "");
    }
}
