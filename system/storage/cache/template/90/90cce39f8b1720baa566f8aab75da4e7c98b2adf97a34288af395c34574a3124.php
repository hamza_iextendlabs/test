<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/report.twig */
class __TwigTemplate_4a1934608da9390e2a35d782c2d3273af9a046040a1a075b0055c43ef109bf0a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 24
        echo ($context["text_edit"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 27
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 29
        echo ($context["entry_status"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"module_report_status\" id=\"input-status\" class=\"form-control\">
                ";
        // line 32
        if (($context["module_report_status"] ?? null)) {
            // line 33
            echo "                <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\">";
            // line 34
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        } else {
            // line 36
            echo "                <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 37
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        }
        // line 39
        echo "              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class=\"container-fluid\">
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"> ";
        // line 50
        echo ($context["text_view"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <li><a href=\"#content1\"> <button type=\"button\" class=\"btn btn-primary btn-sm\">Report Related to Customer</button> </a></li><br>
        <li><a href=\"#content2\"> <button type=\"button\" class=\"btn btn-primary btn-sm\">Report Related to Ordered of Product</button> </a></li><br>
        <li><a href=\"#content3\"> <button type=\"button\" class=\"btn btn-primary btn-sm\">Report Related to Return of Prodect</button> </a></li><br>
        <li><a href=\"#content4\"> <button type=\"button\" class=\"btn btn-primary btn-sm\">Report Related to Prodect</button> </a></li><br>
        <li><a href=\"#content5\"> <button type=\"button\" class=\"btn btn-primary btn-sm\">Report Related to Category</button> </a></li><br>
      </div>
    </div>
  </div>

  <div id=\"content1\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 65
        echo ($context["category_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 70
        echo ($context["text_category"] ?? null);
        echo "</h3>
        </div>
      <div class=\"panel-body\">
        <div class=\"container\">
            <div class=\"container\">
            <h4> Total Category: <b> ";
        // line 75
        echo ($context["total_category"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Enable Category: <b> ";
        // line 76
        echo ($context["enable_category"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Disable Category: <b> ";
        // line 77
        echo ($context["disable_category"] ?? null);
        echo " </b></h4> <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id=\"content2\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 88
        echo ($context["customer_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 93
        echo ($context["text_customer"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
            <h4> Total Customer: <b> ";
        // line 98
        echo ($context["total_customer"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Login Customer: <b> ";
        // line 99
        echo ($context["login_customer"] ?? null);
        echo " </b></h4> <br><hr>
            </div>
          </div>
        </div>
     </div>
    </div>
  </div>

  <div id=\"content3\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 110
        echo ($context["order_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 115
        echo ($context["text_order"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
            <h4> Total Order: <b> ";
        // line 120
        echo ($context["total_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Ordered Product: <b> ";
        // line 121
        echo ($context["total_ordered_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Expired Order: <b> ";
        // line 122
        echo ($context["expired_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Processed Order: <b> ";
        // line 123
        echo ($context["processed_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Pending Order: <b> ";
        // line 124
        echo ($context["pending_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Failed Order: <b> ";
        // line 125
        echo ($context["failed_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Complete Order: <b> ";
        // line 126
        echo ($context["complete_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Canceled Order: <b> ";
        // line 127
        echo ($context["canceled_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> ChargeBack Order: <b> ";
        // line 128
        echo ($context["chargeback_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Return Product: <b> ";
        // line 129
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id=\"content4\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 140
        echo ($context["product_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\">";
        // line 145
        echo ($context["text_product"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <h4> Total Product: <b> ";
        // line 149
        echo ($context["total_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Ordered Product: <b> ";
        // line 150
        echo ($context["total_ordered_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Wishlist Product: <b> ";
        // line 151
        echo ($context["total_wishlist_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Return Product: <b> ";
        // line 152
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Cart Product: <b> ";
        // line 153
        echo ($context["total_cart_product"] ?? null);
        echo " </b></h4><hr>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id=\"content5\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 163
        echo ($context["return_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 168
        echo ($context["text_return"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
              <h4> Total Return Product: <b> ";
        // line 173
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Pending Return Product: <b> ";
        // line 174
        echo ($context["pending_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Complete Return Product: <b> ";
        // line 175
        echo ($context["complete_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Awaiting Return Product: <b> ";
        // line 176
        echo ($context["awaiting_return_product"] ?? null);
        echo " </b></h4> <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
";
        // line 184
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/report.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  387 => 184,  376 => 176,  372 => 175,  368 => 174,  364 => 173,  356 => 168,  348 => 163,  335 => 153,  331 => 152,  327 => 151,  323 => 150,  319 => 149,  312 => 145,  304 => 140,  290 => 129,  286 => 128,  282 => 127,  278 => 126,  274 => 125,  270 => 124,  266 => 123,  262 => 122,  258 => 121,  254 => 120,  246 => 115,  238 => 110,  224 => 99,  220 => 98,  212 => 93,  204 => 88,  190 => 77,  186 => 76,  182 => 75,  174 => 70,  166 => 65,  148 => 50,  135 => 39,  130 => 37,  125 => 36,  120 => 34,  115 => 33,  113 => 32,  107 => 29,  102 => 27,  96 => 24,  92 => 22,  84 => 18,  82 => 17,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/report.twig", "");
    }
}
