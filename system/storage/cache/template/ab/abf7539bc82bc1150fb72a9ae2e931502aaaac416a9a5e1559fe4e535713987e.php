<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* report/order.twig */
class __TwigTemplate_837ed550e964ce6928b73c4a72591aea44284afd5bfda06c173ddc4e80e08990 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <a href=\"";
        // line 6
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_back"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1 style=\"color:DodgerBlue;\">";
        // line 7
        echo ($context["order_title"] ?? null);
        echo "</h1>
    </div>
  </div>
    <div class=\"container-fluid\">
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"> ";
        // line 13
        echo ($context["text_order"] ?? null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">
                <div class=\"row\">
            <div class=\"col-sm-12\">
            <table class=\"table table-bordered table-striped\">
              <thead>
                <tr>
                    <td class=\"text-left\">Date </td>
                    <td class=\"text-left\">Order Id </td>
                    <td class=\"text-left\">Prodect Name </td>
                    <td class=\"text-left\">Quantity </td>
                    <td class=\"text-left\">Price of Prodect </td>
                    <td class=\"text-left\">Tax </td>
                    <td class=\"text-left\">Total Price </td>
                </tr>
              </thead>
              <tbody>
              ";
        // line 31
        if (($context["stocks"] ?? null)) {
            // line 32
            echo "              ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["stocks"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["stock"]) {
                // line 33
                echo "                <tr>
                    <td class=\"text-left\">Date </td>
                    <td class=\"text-left\">Order Id </td>
                    <td class=\"text-left\">Prodect Name </td>
                    <td class=\"text-left\">Quantity </td>
                    <td class=\"text-left\">Price of Prodect </td>
                    <td class=\"text-left\">Tax </td>
                    <td class=\"text-left\">Total Price </td>
                </tr>
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "              ";
        }
        // line 44
        echo "              </tbody>
            </table>
            
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
";
        // line 53
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "report/order.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 53,  107 => 44,  104 => 43,  89 => 33,  84 => 32,  82 => 31,  61 => 13,  52 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "report/order.twig", "");
    }
}
