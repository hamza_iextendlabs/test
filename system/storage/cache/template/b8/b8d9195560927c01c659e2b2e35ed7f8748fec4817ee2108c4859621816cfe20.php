<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* report/product.twig */
class __TwigTemplate_d6e9b13817a86f8352cf092d57caa83eea850aaa241f7ee5bcfc462bfa4d8b1f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <a href=\"";
        // line 6
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 7
        echo ($context["product_title"] ?? null);
        echo "</h1>
    </div>
  </div>
  <div class=\"container-fluid\">
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\">";
        // line 13
        echo ($context["text_product"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <div class=\"container\">
            <h4> Total Product: <b> ";
        // line 17
        echo ($context["total_product"] ?? null);
        echo " </b></h4> <br>
            <h4> Total Ordered Product: <b> ";
        // line 18
        echo ($context["total_ordered_product"] ?? null);
        echo " </b></h4> <br>
            <h4> Total Wishlist Product: <b> ";
        // line 19
        echo ($context["total_wishlist_product"] ?? null);
        echo " </b></h4> <br>
            <h4> Total Return Product: <b> ";
        // line 20
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <br>
            <h4> Total Cart Product: <b> ";
        // line 21
        echo ($context["total_cart_product"] ?? null);
        echo " </b></h4>
        </div>
      </div>
    </div>
  </div>
</div>
";
        // line 27
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "report/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 27,  84 => 21,  80 => 20,  76 => 19,  72 => 18,  68 => 17,  61 => 13,  52 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "report/product.twig", "");
    }
}
