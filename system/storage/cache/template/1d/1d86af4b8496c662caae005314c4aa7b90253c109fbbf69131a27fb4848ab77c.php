<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/test/test.twig */
class __TwigTemplate_0e696660b807dca3b00a9d9c62ec51e5e9ca567c65720420a0202150616fce81 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
              ";
        // line 2
        if (($context["seller_status"] ?? null)) {
            // line 3
            echo "                Status: Enable
                ";
        } else {
            // line 5
            echo "                Status: Disable
              ";
        }
        // line 7
        echo "                <br>
              ";
        // line 8
        if (($context["study"] ?? null)) {
            // line 9
            echo "                You have learned php.
                ";
        } else {
            // line 11
            echo "                You have not learned php.
              ";
        }
        // line 13
        echo "              <br>
              You select the ";
        // line 14
        echo ($context["course"] ?? null);
        echo " course.";
    }

    public function getTemplateName()
    {
        return "default/template/test/test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 14,  63 => 13,  59 => 11,  55 => 9,  53 => 8,  50 => 7,  46 => 5,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/test/test.twig", "");
    }
}
