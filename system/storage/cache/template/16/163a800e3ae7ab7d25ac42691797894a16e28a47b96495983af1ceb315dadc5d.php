<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/test/test.twig */
class __TwigTemplate_733b91d6a254bab068cb7b82c7bfb7568e8841a4226bcfbc0fa16647e40ed113 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
              ";
        // line 2
        if (($context["seller_status"] ?? null)) {
            // line 3
            echo "                Status: Enable
                ";
        } else {
            // line 5
            echo "                Status: Disable
              ";
        }
        // line 7
        echo "                <br>


              ";
        // line 10
        if (($context["study"] ?? null)) {
            // line 11
            echo "                You have learned php.
                ";
        } else {
            // line 13
            echo "                You have not learned php.
              ";
        }
        // line 15
        echo "                <br>


              ";
        // line 18
        if ((($context["course"] ?? null) == "Javascript")) {
            // line 19
            echo "                You select the ";
            echo ($context["course"] ?? null);
            echo " course.
              ";
        }
        // line 21
        echo "              ";
        if ((($context["course"] ?? null) == "PHP")) {
            // line 22
            echo "                You select the ";
            echo ($context["course"] ?? null);
            echo " course.
              ";
        }
        // line 24
        echo "                
                <br>
              Message: ";
        // line 26
        echo ($context["msg"] ?? null);
        echo "
              ";
    }

    public function getTemplateName()
    {
        return "default/template/test/test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 26,  87 => 24,  81 => 22,  78 => 21,  72 => 19,  70 => 18,  65 => 15,  61 => 13,  57 => 11,  55 => 10,  50 => 7,  46 => 5,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/test/test.twig", "");
    }
}
