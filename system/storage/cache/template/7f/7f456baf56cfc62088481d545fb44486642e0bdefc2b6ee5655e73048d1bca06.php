<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/report.twig */
class __TwigTemplate_61fd8fb205871b509ddc315da2bf880dd7335c9a41071928701121fdfcba3050 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 24
        echo ($context["text_edit"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 27
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 29
        echo ($context["entry_status"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"module_report_status\" id=\"input-status\" class=\"form-control\">
                ";
        // line 32
        if (($context["module_report_status"] ?? null)) {
            // line 33
            echo "                <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\">";
            // line 34
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        } else {
            // line 36
            echo "                <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 37
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        }
        // line 39
        echo "              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  ";
        // line 63
        echo "
  <div id=\"content1\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 67
        echo ($context["category_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 72
        echo ($context["text_category"] ?? null);
        echo "</h3>
        </div>
      <div class=\"panel-body\">
        <div class=\"container\">
            <div class=\"container\">
            <h3> Total Category:  ";
        // line 77
        echo ($context["total_category"] ?? null);
        echo " </b></h3> <br>
            <div class=\"row\">
            <div class=\"col-sm-6\">
            <table class=\"table table-bordered table-striped\">
              <thead>
                <tr>
                    <td class=\"text-left\">Category Name </td>
                    <td class=\"text-left\">No. of Product </td>
                </tr>
              </thead>
              <tbody>
              ";
        // line 88
        if (($context["categorys"] ?? null)) {
            // line 89
            echo "              ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categorys"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 90
                echo "                <tr>
                    <td class=\"text-left\">";
                // line 91
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 91);
                echo "</td>
                    <td class=\"text-left\">";
                // line 92
                echo twig_get_attribute($this->env, $this->source, $context["category"], "product_in_category", [], "any", false, false, false, 92);
                echo "</td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 95
            echo "              ";
        }
        // line 96
        echo "              </tbody>
            </table>
            </div>
            </div>
            <hr>
            
            <h3> <b>Low Stock Product </b></h3> <br>
            <div class=\"row\">
            <div class=\"col-sm-6\">
            <table class=\"table table-bordered table-striped\">
              <thead>
                <tr>
                    <td class=\"text-left\">Prodect Name </td>
                    <td class=\"text-left\">Quantity of Product </td>
                </tr>
              </thead>
              <tbody>
              ";
        // line 113
        if (($context["lowstocks"] ?? null)) {
            // line 114
            echo "              ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["lowstocks"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["lowstock"]) {
                // line 115
                echo "                <tr>
                    <td class=\"text-left\">";
                // line 116
                echo twig_get_attribute($this->env, $this->source, $context["lowstock"], "name", [], "any", false, false, false, 116);
                echo "</td>
                    <td class=\"text-left\">";
                // line 117
                echo twig_get_attribute($this->env, $this->source, $context["lowstock"], "quantity", [], "any", false, false, false, 117);
                echo "</td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lowstock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 120
            echo "              ";
        }
        // line 121
        echo "              </tbody>
            </table>
            </div>
            </div>
            <hr>


            <h4> Enable Category: <b> ";
        // line 128
        echo ($context["enable_category"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Disable Category: <b> ";
        // line 129
        echo ($context["disable_category"] ?? null);
        echo " </b></h4> <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id=\"content2\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 140
        echo ($context["customer_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 145
        echo ($context["text_customer"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
            <h4> Total Customer: <b> ";
        // line 150
        echo ($context["total_customer"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Login Customer: <b> ";
        // line 151
        echo ($context["login_customer"] ?? null);
        echo " </b></h4> <br><hr>
            </div>
          </div>
        </div>
     </div>
    </div>
  </div>

  <div id=\"content3\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 162
        echo ($context["order_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 167
        echo ($context["text_order"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
            <h4> Total Order: <b> ";
        // line 172
        echo ($context["total_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Ordered Product: <b> ";
        // line 173
        echo ($context["total_ordered_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Expired Order: <b> ";
        // line 174
        echo ($context["expired_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Processed Order: <b> ";
        // line 175
        echo ($context["processed_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Pending Order: <b> ";
        // line 176
        echo ($context["pending_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Failed Order: <b> ";
        // line 177
        echo ($context["failed_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Complete Order: <b> ";
        // line 178
        echo ($context["complete_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Canceled Order: <b> ";
        // line 179
        echo ($context["canceled_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> ChargeBack Order: <b> ";
        // line 180
        echo ($context["chargeback_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Return Product: <b> ";
        // line 181
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id=\"content4\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 192
        echo ($context["product_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\">";
        // line 197
        echo ($context["text_product"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <h4> Total Product: <b> ";
        // line 201
        echo ($context["total_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Ordered Product: <b> ";
        // line 202
        echo ($context["total_ordered_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Wishlist Product: <b> ";
        // line 203
        echo ($context["total_wishlist_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Return Product: <b> ";
        // line 204
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Cart Product: <b> ";
        // line 205
        echo ($context["total_cart_product"] ?? null);
        echo " </b></h4><hr>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id=\"content5\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 215
        echo ($context["return_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 220
        echo ($context["text_return"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
              <h4> Total Return Product: <b> ";
        // line 225
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Pending Return Product: <b> ";
        // line 226
        echo ($context["pending_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Complete Return Product: <b> ";
        // line 227
        echo ($context["complete_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Awaiting Return Product: <b> ";
        // line 228
        echo ($context["awaiting_return_product"] ?? null);
        echo " </b></h4> <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
";
        // line 236
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/report.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  462 => 236,  451 => 228,  447 => 227,  443 => 226,  439 => 225,  431 => 220,  423 => 215,  410 => 205,  406 => 204,  402 => 203,  398 => 202,  394 => 201,  387 => 197,  379 => 192,  365 => 181,  361 => 180,  357 => 179,  353 => 178,  349 => 177,  345 => 176,  341 => 175,  337 => 174,  333 => 173,  329 => 172,  321 => 167,  313 => 162,  299 => 151,  295 => 150,  287 => 145,  279 => 140,  265 => 129,  261 => 128,  252 => 121,  249 => 120,  240 => 117,  236 => 116,  233 => 115,  228 => 114,  226 => 113,  207 => 96,  204 => 95,  195 => 92,  191 => 91,  188 => 90,  183 => 89,  181 => 88,  167 => 77,  159 => 72,  151 => 67,  145 => 63,  135 => 39,  130 => 37,  125 => 36,  120 => 34,  115 => 33,  113 => 32,  107 => 29,  102 => 27,  96 => 24,  92 => 22,  84 => 18,  82 => 17,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/report.twig", "");
    }
}
