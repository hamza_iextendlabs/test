<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/test/test.twig */
class __TwigTemplate_8e45dc9eb58ff1f53db0320660b5564aba17138a7a65ce135652658790287d5d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
              <div class= \"jumbotorn\">
              ";
        // line 3
        if (($context["seller_status"] ?? null)) {
            // line 4
            echo "                Status: Enable
                ";
        } else {
            // line 6
            echo "                Status: Disable
              ";
        }
        // line 8
        echo "                <br>


              ";
        // line 11
        if (($context["study"] ?? null)) {
            // line 12
            echo "                You have learned php.
                ";
        } else {
            // line 14
            echo "                You have not learned php.
              ";
        }
        // line 16
        echo "                <br>


              ";
        // line 19
        if (($context["course_1"] ?? null)) {
            // line 20
            echo "                You select the ";
            echo ($context["course_1"] ?? null);
            echo " course.
              ";
        }
        // line 22
        echo "                <br>
              ";
        // line 23
        if (($context["course_2"] ?? null)) {
            // line 24
            echo "                You select the ";
            echo ($context["course_2"] ?? null);
            echo " course.
              ";
        }
        // line 26
        echo "                
                <br>
              ";
        // line 28
        if (($context["msg"] ?? null)) {
            // line 29
            echo "                Message: ";
            echo ($context["msg"] ?? null);
            echo "
              ";
        }
        // line 31
        echo "              <div>";
    }

    public function getTemplateName()
    {
        return "default/template/test/test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 31,  96 => 29,  94 => 28,  90 => 26,  84 => 24,  82 => 23,  79 => 22,  73 => 20,  71 => 19,  66 => 16,  62 => 14,  58 => 12,  56 => 11,  51 => 8,  47 => 6,  43 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/test/test.twig", "");
    }
}
