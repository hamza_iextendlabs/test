<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/report.twig */
class __TwigTemplate_0a1793267166e221783bf486b01969ba1bd668cbe33cc72650879923985094c9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  

  <div class=\"container-fluid\">
    <div>
      <h1 style=\"color:DodgerBlue;\">";
        // line 20
        echo ($context["category_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"row\">
      <div class=\"col-sm-8\">
        <div class=\"container-fluid\">
          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <p style = \"text-align: left\" class=\"panel-title\"> ";
        // line 27
        echo ($context["text_category"] ?? null);
        echo "</p>
              <p style = \"text-align: right\" > <b>Total Category:  ";
        // line 28
        echo ($context["total_category"] ?? null);
        echo " </b></p>
            </div>
            <table class=\"table table-bordered table-striped\">
              <thead>
                <tr>
                  <td class=\"text-left\">Category Name </td>
                  <td class=\"text-left\">No. of Product </td>
                </tr>
              </thead>
              <tbody>
                ";
        // line 38
        if (($context["categorys"] ?? null)) {
            // line 39
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categorys"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 40
                echo "                <tr>
                  <td class=\"text-left\">";
                // line 41
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 41);
                echo "</td>
                  <td class=\"text-left\">";
                // line 42
                echo twig_get_attribute($this->env, $this->source, $context["category"], "product_in_category", [], "any", false, false, false, 42);
                echo "</td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "                ";
        }
        // line 46
        echo "              </tbody>
            </table>
          </div>
        </div>
      </div>
      
      <div class=\"col-sm-4\">
          <div class=\"panel-body\">
            <div class=\"panel panel-default\">
              <div class=\"panel-heading\">
                <h3 class=\"panel-title\"> ";
        // line 56
        echo ($context["text_quantity"] ?? null);
        echo "</h3>
              </div>
              <div class=\"container-fluid\">
                <div class=\"form-group\">
                  <label for=\"input-category\" class=\"control-label\">";
        // line 60
        echo ($context["select_category"] ?? null);
        echo "</label>
                  <select name=\"category\" class=\"form-control\">
                      ";
        // line 62
        if (($context["categorys"] ?? null)) {
            // line 63
            echo "                      ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categorys"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 64
                echo "                      <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "category_id", [], "any", false, false, false, 64);
                echo "\" id=\"input-category\" class=\"form-control\">";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 64);
                echo " </option>
                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "                      ";
        }
        // line 67
        echo "                  </select>
                  <label class=\"control-label\" for=\"input-quantity\">";
        // line 68
        echo ($context["entry_quantity"] ?? null);
        echo "</label>
                  <input type=\"text\" name=\"quantity\" value=\"";
        // line 69
        echo ($context["quantity"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_quantity"] ?? null);
        echo "\" id=\"input-quantity\" class=\"form-control\" />
                </div>
                <div class=\"form-group text-right\">
                  <button type=\"button\" id=\"button-quantity\" class=\"btn btn-default\"><i class=\"fa fa-filter\"></i> ";
        // line 72
        echo ($context["button_filter"] ?? null);
        echo " </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-sm-4\">
          <div class=\"panel-body\">
            <div class=\"panel panel-default\">
              <div class=\"panel-heading\">
                <h3 class=\"panel-title\"> ";
        // line 82
        echo ($context["text_ordered"] ?? null);
        echo "</h3>
              </div>
              <div class=\"container-fluid\">
                <div class=\"container-fluid\">
                  <div class=\"form-group\">
                    <label for=\"input-date-start\" class=\"col-sm-6 control-label\">";
        // line 87
        echo ($context["date_start"] ?? null);
        echo "</label>
                    <div class=\"col-sm-12\">
                      <div class=\"input-group datetime\">
                        <input type=\"text\" name=\"start\" value=\"";
        // line 90
        echo ($context["start"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["date_start"] ?? null);
        echo "\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" id=\"input-date-start\" class=\"form-control\" class=\"col-sm-2\"/> <span class=\"input-group-btn\">
                        <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class=\"container-fluid\">
                  <div class=\"form-group\">
                    <label for=\"input-date-end\" class=\"col-sm-6 control-label\">";
        // line 99
        echo ($context["date_end"] ?? null);
        echo "</label>
                    <div class=\"col-sm-12\">
                      <div class=\"input-group datetime\">
                        <input type=\"text\" name=\"end\" value=\"";
        // line 102
        echo ($context["end"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["date_end"] ?? null);
        echo "\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" id=\"input-date-end\" class=\"form-control\" class=\"col-sm-2\"/> <span class=\"input-group-btn\">
                        <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class=\"form-group text-right\">
                  <button type=\"button\" id=\"button-order\" class=\"btn btn-default\"><i class=\"fa fa-filter\"></i> ";
        // line 110
        echo ($context["button_filter"] ?? null);
        echo " </button>
                </div>
              </div>
            </div>
          </div>
        </div>
  </div>
</div>
  



";
        // line 224
        echo ($context["footer"] ?? null);
        echo "

<script type=\"text/javascript\"><!--
\$('#button-quantity').on('click', function() {
\tvar url = '';

  var category = \$('select[name=\\'category\\']').val();

\tif (category) {
\t\turl += '&category=' + encodeURIComponent(category);
\t}
  
  var quantity = \$('input[name=\\'quantity\\']').val();

\tif (quantity) {
\t\turl += '&quantity=' + encodeURIComponent(quantity);
\t}
  
  \tlocation = 'index.php?route=extension/module/report/lowstock&user_token=";
        // line 242
        echo ($context["user_token"] ?? null);
        echo "' + url;
});
//--></script>

<script type=\"text/javascript\"><!--
\$('#button-order').on('click', function() {
\tvar url = '';

  var date_start = \$('input[name=\\'start\\']').val();

\tif (date_start) {
\t\turl += '&date_start=' + encodeURIComponent(date_start);
\t}
  
  var date_end = \$('input[name=\\'end\\']').val();

\tif (date_end) {
\t\turl += '&date_end=' + encodeURIComponent(date_end);
\t}
  
  \tlocation = 'index.php?route=extension/module/report/order&user_token=";
        // line 262
        echo ($context["user_token"] ?? null);
        echo "' + url;
});
//--></script>
  <script type=\"text/javascript\"><!--
\$('.datetime').datetimepicker({
\tlanguage: '";
        // line 267
        echo ($context["datepicker"] ?? null);
        echo "',
\tpickDate: true,
\tpickTime: true
});
//--></script> ";
    }

    public function getTemplateName()
    {
        return "extension/module/report.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  325 => 267,  317 => 262,  294 => 242,  273 => 224,  258 => 110,  245 => 102,  239 => 99,  225 => 90,  219 => 87,  211 => 82,  198 => 72,  190 => 69,  186 => 68,  183 => 67,  180 => 66,  169 => 64,  164 => 63,  162 => 62,  157 => 60,  150 => 56,  138 => 46,  135 => 45,  126 => 42,  122 => 41,  119 => 40,  114 => 39,  112 => 38,  99 => 28,  95 => 27,  85 => 20,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/report.twig", "");
    }
}
