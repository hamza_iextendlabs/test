<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/test/test.twig */
class __TwigTemplate_8d91b941fe83a311b812fc70856b5543d7cd74f2b5e6fd0476d972add06161c7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "           <form>
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 2
        echo ($context["entry_status"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"test_status\" id=\"input-status\" class=\"form-control\">
                <option value=\"1\" selected=\"selected\">";
        // line 5
        echo ($context["text_enabled"] ?? null);
        echo "</option>
                <option value=\"0\">";
        // line 6
        echo ($context["text_disabled"] ?? null);
        echo "</option>
              </select>
              <input type=\"submit\" value=\"Submit\">
              </form>
              ";
        // line 10
        if (($context["test_status"] ?? null)) {
            // line 11
            echo "                Hallo
                ";
        } else {
            // line 13
            echo "                hallo
              ";
        }
    }

    public function getTemplateName()
    {
        return "default/template/test/test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 13,  59 => 11,  57 => 10,  50 => 6,  46 => 5,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/test/test.twig", "");
    }
}
