<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/news/news.twig */
class __TwigTemplate_13458f7cea86bf91d4124ae672bacd880acc8ab7c755bec372d2cc39c315ab69 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"news-news\" class=\"container\">
  
  <div class=\"row\">";
        // line 4
        echo ($context["column_left"] ?? null);
        echo "
    ";
        // line 5
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 6
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 7
            echo "    ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 8
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 9
            echo "    ";
        } else {
            // line 10
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 11
            echo "    ";
        }
        // line 12
        echo "    <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "
      <u><h1> ";
        // line 13
        echo ($context["heading_title"] ?? null);
        echo " </h1></u>
         <ul>
          <li> <h3>";
        // line 15
        echo ($context["text_title"] ?? null);
        echo " </h3>
          <p> ";
        // line 16
        echo ($context["title"] ?? null);
        echo " </p> </li>
          <li><h3>";
        // line 17
        echo ($context["text_description"] ?? null);
        echo " </h3>
          <p> ";
        // line 18
        echo ($context["description"] ?? null);
        echo " </p></li>
          </ul>
          <div class=\"pull-right\"><a href=\"";
        // line 20
        echo ($context["allnews"] ?? null);
        echo "\" class=\"btn btn-primary\">";
        echo ($context["button_all_news"] ?? null);
        echo "</a></div>
      ";
        // line 21
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 22
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
";
        // line 24
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/news/news.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 24,  106 => 22,  102 => 21,  96 => 20,  91 => 18,  87 => 17,  83 => 16,  79 => 15,  74 => 13,  67 => 12,  64 => 11,  61 => 10,  58 => 9,  55 => 8,  52 => 7,  49 => 6,  47 => 5,  43 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/news/news.twig", "");
    }
}
