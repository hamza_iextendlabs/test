<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* report/lowstock.twig */
class __TwigTemplate_c716c2fbbd709e123c189e7852ddfcfc588903b29fa39fa0f6c88291ea17b297 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <a href=\"";
        // line 6
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_back"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1 style=\"color:DodgerBlue;\">";
        // line 7
        echo ($context["lowstock_title"] ?? null);
        echo "</h1>
    </div>
  </div>
  <div class=\"container-fluid\">
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"> ";
        // line 13
        echo ($context["text_lowstock"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <div class=\"container\">
            <div class=\"container\">
            <h4> Total Customer: <b> ";
        // line 18
        echo ($context["total_customer"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Login Customer: <b> ";
        // line 19
        echo ($context["login_customer"] ?? null);
        echo " </b></h4> <br><hr>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
";
        // line 26
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "report/lowstock.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 26,  73 => 19,  69 => 18,  61 => 13,  52 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "report/lowstock.twig", "");
    }
}
