<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/report.twig */
class __TwigTemplate_ed9d4b671a0728d3537dc9be9812715964cc0c4cde4694bf86b15c7a44b63344 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  
  <div id=\"content1\">
    <div class=\"page-header\">
      <div class=\"container-fluid\">
        <h1 style=\"color:DodgerBlue;\">";
        // line 20
        echo ($context["category_title"] ?? null);
        echo "</h1>
      </div>
      <div class=\"row\">
        <div class=\"col-sm-8\">
          <div class=\"container-fluid\">
            <div class=\"panel panel-default\">
              <div class=\"panel-heading\">
                <h3 class=\"panel-title\"> ";
        // line 27
        echo ($context["text_category"] ?? null);
        echo "</h3>
              </div>
                <h3> <b>Total Category:  ";
        // line 29
        echo ($context["total_category"] ?? null);
        echo " </b></h3> <br>
                  <table class=\"table table-bordered table-striped\">
                    <thead>
                      <tr>
                        <td class=\"text-left\">Category Name </td>
                        <td class=\"text-left\">No. of Product </td>
                      </tr>
                    </thead>
                    <tbody>
                      ";
        // line 38
        if (($context["categorys"] ?? null)) {
            // line 39
            echo "                      ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categorys"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 40
                echo "                      <tr>
                        <td class=\"text-left\">";
                // line 41
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 41);
                echo "</td>
                        <td class=\"text-left\">";
                // line 42
                echo twig_get_attribute($this->env, $this->source, $context["category"], "product_in_category", [], "any", false, false, false, 42);
                echo "</td>
                      </tr>
                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "                      ";
        }
        // line 46
        echo "                    </tbody>
                  </table>
            </div>
          </div>
        </div>
        <div class=\"col-sm-4\">
          <div class=\"panel-body\">
            <div class=\"panel panel-default\">
              <div class=\"panel-heading\">
                <h3 class=\"panel-title\"> ";
        // line 55
        echo ($context["text_quantity"] ?? null);
        echo "</h3>
              </div>
              <div class=\"container-fluid\">
                <div class=\"form-group\">
                  <label class=\"control-label\" for=\"input-quantity\">";
        // line 59
        echo ($context["entry_quantity"] ?? null);
        echo "</label>
                  <label for=\"input-category\" class=\"control-label\">";
        // line 60
        echo ($context["select_category"] ?? null);
        echo "</label>
                  <select name=\"category\" class=\"form-control\">
                      ";
        // line 62
        if (($context["categorys"] ?? null)) {
            // line 63
            echo "                      ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categorys"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 64
                echo "                      <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "category_id", [], "any", false, false, false, 64);
                echo "\" id=\"input-category\" class=\"form-control\">";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 64);
                echo " </option>
                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "                      ";
        }
        // line 67
        echo "                  </select>
                  <input type=\"text\" name=\"quantity\" value=\"";
        // line 68
        echo ($context["quantity"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_quantity"] ?? null);
        echo "\" id=\"input-quantity\" class=\"form-control\" />
                </div>
                <div class=\"form-group text-right\">
                  <button type=\"button\" id=\"button-quantity\" class=\"btn btn-default\"><i class=\"fa fa-filter\"></i> ";
        // line 71
        echo ($context["button_filter"] ?? null);
        echo " </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        ";
        // line 101
        echo "      </div>
    </div>
  </div>

  <div id=\"content2\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 108
        echo ($context["customer_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 113
        echo ($context["text_customer"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
            <h4> Total Customer: <b> ";
        // line 118
        echo ($context["total_customer"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Login Customer: <b> ";
        // line 119
        echo ($context["login_customer"] ?? null);
        echo " </b></h4> <br><hr>
            </div>
          </div>
        </div>
     </div>
    </div>
  </div>

  <div id=\"content3\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 130
        echo ($context["order_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 135
        echo ($context["text_order"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
            <h4> Total Order: <b> ";
        // line 140
        echo ($context["total_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Ordered Product: <b> ";
        // line 141
        echo ($context["total_ordered_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Expired Order: <b> ";
        // line 142
        echo ($context["expired_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Processed Order: <b> ";
        // line 143
        echo ($context["processed_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Pending Order: <b> ";
        // line 144
        echo ($context["pending_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Failed Order: <b> ";
        // line 145
        echo ($context["failed_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Complete Order: <b> ";
        // line 146
        echo ($context["complete_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Canceled Order: <b> ";
        // line 147
        echo ($context["canceled_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> ChargeBack Order: <b> ";
        // line 148
        echo ($context["chargeback_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Return Product: <b> ";
        // line 149
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id=\"content4\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 160
        echo ($context["product_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\">";
        // line 165
        echo ($context["text_product"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <h4> Total Product: <b> ";
        // line 169
        echo ($context["total_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Ordered Product: <b> ";
        // line 170
        echo ($context["total_ordered_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Wishlist Product: <b> ";
        // line 171
        echo ($context["total_wishlist_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Return Product: <b> ";
        // line 172
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Cart Product: <b> ";
        // line 173
        echo ($context["total_cart_product"] ?? null);
        echo " </b></h4><hr>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id=\"content5\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 183
        echo ($context["return_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 188
        echo ($context["text_return"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
              <h4> Total Return Product: <b> ";
        // line 193
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Pending Return Product: <b> ";
        // line 194
        echo ($context["pending_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Complete Return Product: <b> ";
        // line 195
        echo ($context["complete_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Awaiting Return Product: <b> ";
        // line 196
        echo ($context["awaiting_return_product"] ?? null);
        echo " </b></h4> <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
";
        // line 204
        echo ($context["footer"] ?? null);
        echo "

<script type=\"text/javascript\"><!--
\$('#button-quantity').on('click', function() {
\tvar url = '';

  var category = \$('select[name=\\'category\\']').val();

\tif (category) {
\t\turl += '&category=' + encodeURIComponent(category);
\t}
  
  var quantity = \$('input[name=\\'quantity\\']').val();

\tif (quantity) {
\t\turl += '&quantity=' + encodeURIComponent(quantity);
\t}
  
  \tlocation = 'index.php?route=extension/module/report/lowstock&user_token=";
        // line 222
        echo ($context["user_token"] ?? null);
        echo "' + url;
});
//--></script>

<script type=\"text/javascript\"><!--
\$('#button-category').on('click', function() {
\tvar url = '';
  
  var category = \$('select[name=\\'category\\']').val();

\tif (category) {
\t\turl += '&category=' + encodeURIComponent(category);
\t}
  \tlocation = 'index.php?route=extension/module/report/categoryquantity&user_token=";
        // line 235
        echo ($context["user_token"] ?? null);
        echo "' + url;
});
//--></script>";
    }

    public function getTemplateName()
    {
        return "extension/module/report.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  435 => 235,  419 => 222,  398 => 204,  387 => 196,  383 => 195,  379 => 194,  375 => 193,  367 => 188,  359 => 183,  346 => 173,  342 => 172,  338 => 171,  334 => 170,  330 => 169,  323 => 165,  315 => 160,  301 => 149,  297 => 148,  293 => 147,  289 => 146,  285 => 145,  281 => 144,  277 => 143,  273 => 142,  269 => 141,  265 => 140,  257 => 135,  249 => 130,  235 => 119,  231 => 118,  223 => 113,  215 => 108,  206 => 101,  197 => 71,  189 => 68,  186 => 67,  183 => 66,  172 => 64,  167 => 63,  165 => 62,  160 => 60,  156 => 59,  149 => 55,  138 => 46,  135 => 45,  126 => 42,  122 => 41,  119 => 40,  114 => 39,  112 => 38,  100 => 29,  95 => 27,  85 => 20,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/report.twig", "");
    }
}
