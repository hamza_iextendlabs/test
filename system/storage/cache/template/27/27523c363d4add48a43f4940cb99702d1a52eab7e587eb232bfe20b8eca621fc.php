<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/report.twig */
class __TwigTemplate_bcc83300b865b5109d3b52c501a00581c7abb6f1c3286b43031809b6a13302f5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  
  <div id=\"content1\">
    <div class=\"page-header\">
      <div class=\"container\">
        <h1 style=\"color:DodgerBlue;\">";
        // line 20
        echo ($context["category_title"] ?? null);
        echo "</h1>
      </div>
      <div class=\"row\">
        <div class=\"col-sm-8\">
          <div class=\"container-fluid\">
            <div class=\"panel panel-default\">
              <div class=\"panel-heading\">
                <h3 class=\"panel-title\"> ";
        // line 27
        echo ($context["text_category"] ?? null);
        echo "</h3>
              </div>
              <div class=\"panel-body\">
                <div class=\"container\">
                  <h3> <b>Total Category:  ";
        // line 31
        echo ($context["total_category"] ?? null);
        echo " </b></h3> <br>
                  <div class=\"row\">
                    <div class=\"col-sm-7\">
                      <table class=\"table table-bordered table-striped\">
                        <thead>
                          <tr>
                            <td class=\"text-left\">Category Name </td>
                            <td class=\"text-left\">No. of Product </td>
                          </tr>
                        </thead>
                        <tbody>
                          ";
        // line 42
        if (($context["categorys"] ?? null)) {
            // line 43
            echo "                          ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categorys"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 44
                echo "                          <tr>
                            <td class=\"text-left\">";
                // line 45
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 45);
                echo "</td>
                            <td class=\"text-left\">";
                // line 46
                echo twig_get_attribute($this->env, $this->source, $context["category"], "product_in_category", [], "any", false, false, false, 46);
                echo "</td>
                          </tr>
                          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 49
            echo "                          ";
        }
        // line 50
        echo "                        </tbody>
                      </table>
                    </div>
                  </div>
                  <hr>
                  <h4> Enable Category: <b> ";
        // line 55
        echo ($context["enable_category"] ?? null);
        echo " </b></h4> <br><hr>
                  <h4> Disable Category: <b> ";
        // line 56
        echo ($context["disable_category"] ?? null);
        echo " </b></h4> <hr>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-sm-4\">
          <div class=\"panel-body\">
            <div class=\"panel panel-default\">
              <div class=\"panel-heading\">
                <h3 class=\"panel-title\"> ";
        // line 66
        echo ($context["text_quantity"] ?? null);
        echo "</h3>
              </div>
              <div class=\"container-fluid\">
                <div class=\"form-group\">
                  <label class=\"control-label\" for=\"input-quantity\">";
        // line 70
        echo ($context["entry_quantity"] ?? null);
        echo "</label>
                  <input type=\"text\" name=\"quantity\" value=\"";
        // line 71
        echo ($context["quantity"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_quantity"] ?? null);
        echo "\" id=\"input-quantity\" class=\"form-control\" />
                </div>
                <div class=\"form-group text-right\">
                  <button type=\"button\" id=\"button-quantity\" class=\"btn btn-default\"><i class=\"fa fa-filter\"></i> ";
        // line 74
        echo ($context["button_filter"] ?? null);
        echo " </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-sm-4\">
          <div class=\"panel-body\">
            <div class=\"panel panel-default\">
              <div class=\"panel-heading\">
                <h3 class=\"panel-title\"> ";
        // line 84
        echo ($context["text_quantity"] ?? null);
        echo "</h3>
              </div>
              <div class=\"container-fluid\">
                <div class=\"form-group\">
                  <label class=\"control-label\" for=\"input-quantity\">";
        // line 88
        echo ($context["entry_quantity"] ?? null);
        echo "</label>
                  <input type=\"text\" name=\"quantity\" value=\"";
        // line 89
        echo ($context["quantity"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_quantity"] ?? null);
        echo "\" id=\"input-quantity\" class=\"form-control\" />
                </div>
                <div class=\"form-group text-right\">
                  <button type=\"button\" id=\"button-quantity\" class=\"btn btn-default\"><i class=\"fa fa-filter\"></i> ";
        // line 92
        echo ($context["button_filter"] ?? null);
        echo " </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id=\"content2\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 105
        echo ($context["customer_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 110
        echo ($context["text_customer"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
            <h4> Total Customer: <b> ";
        // line 115
        echo ($context["total_customer"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Login Customer: <b> ";
        // line 116
        echo ($context["login_customer"] ?? null);
        echo " </b></h4> <br><hr>
            </div>
          </div>
        </div>
     </div>
    </div>
  </div>

  <div id=\"content3\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 127
        echo ($context["order_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 132
        echo ($context["text_order"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
            <h4> Total Order: <b> ";
        // line 137
        echo ($context["total_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Ordered Product: <b> ";
        // line 138
        echo ($context["total_ordered_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Expired Order: <b> ";
        // line 139
        echo ($context["expired_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Processed Order: <b> ";
        // line 140
        echo ($context["processed_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Pending Order: <b> ";
        // line 141
        echo ($context["pending_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Failed Order: <b> ";
        // line 142
        echo ($context["failed_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Complete Order: <b> ";
        // line 143
        echo ($context["complete_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Canceled Order: <b> ";
        // line 144
        echo ($context["canceled_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> ChargeBack Order: <b> ";
        // line 145
        echo ($context["chargeback_order"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Return Product: <b> ";
        // line 146
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id=\"content4\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 157
        echo ($context["product_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\">";
        // line 162
        echo ($context["text_product"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <h4> Total Product: <b> ";
        // line 166
        echo ($context["total_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Ordered Product: <b> ";
        // line 167
        echo ($context["total_ordered_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Wishlist Product: <b> ";
        // line 168
        echo ($context["total_wishlist_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Return Product: <b> ";
        // line 169
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Total Cart Product: <b> ";
        // line 170
        echo ($context["total_cart_product"] ?? null);
        echo " </b></h4><hr>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id=\"content5\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 180
        echo ($context["return_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 185
        echo ($context["text_return"] ?? null);
        echo "</h3>
        </div>
        <div class=\"panel-body\">
          <div class=\"container\">
            <div class=\"container\">
              <h4> Total Return Product: <b> ";
        // line 190
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Pending Return Product: <b> ";
        // line 191
        echo ($context["pending_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Complete Return Product: <b> ";
        // line 192
        echo ($context["complete_return_product"] ?? null);
        echo " </b></h4> <br><hr>
              <h4> Awaiting Return Product: <b> ";
        // line 193
        echo ($context["awaiting_return_product"] ?? null);
        echo " </b></h4> <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
";
        // line 201
        echo ($context["footer"] ?? null);
        echo "

<script type=\"text/javascript\"><!--
\$('#button-quantity').on('click', function() {
\tvar url = '';
  
  var quantity = \$('input[name=\\'quantity\\']').val();

\tif (quantity) {
\t\turl += '&quantity=' + encodeURIComponent(quantity);
\t}
  \tlocation = 'index.php?route=extension/module/report/lowstock&user_token=";
        // line 212
        echo ($context["user_token"] ?? null);
        echo "' + url;
});
//--></script>";
    }

    public function getTemplateName()
    {
        return "extension/module/report.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  430 => 212,  416 => 201,  405 => 193,  401 => 192,  397 => 191,  393 => 190,  385 => 185,  377 => 180,  364 => 170,  360 => 169,  356 => 168,  352 => 167,  348 => 166,  341 => 162,  333 => 157,  319 => 146,  315 => 145,  311 => 144,  307 => 143,  303 => 142,  299 => 141,  295 => 140,  291 => 139,  287 => 138,  283 => 137,  275 => 132,  267 => 127,  253 => 116,  249 => 115,  241 => 110,  233 => 105,  217 => 92,  209 => 89,  205 => 88,  198 => 84,  185 => 74,  177 => 71,  173 => 70,  166 => 66,  153 => 56,  149 => 55,  142 => 50,  139 => 49,  130 => 46,  126 => 45,  123 => 44,  118 => 43,  116 => 42,  102 => 31,  95 => 27,  85 => 20,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/report.twig", "");
    }
}
