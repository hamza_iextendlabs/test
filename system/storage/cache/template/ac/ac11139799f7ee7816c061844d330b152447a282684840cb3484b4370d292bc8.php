<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* report/stock.twig */
class __TwigTemplate_794effa4d4ab4e51f25319e96c2928289086e9d0d56fffbf8e9c9ab13b2941b7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <a href=\"";
        // line 6
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_back"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1 style=\"color:DodgerBlue;\">";
        // line 7
        echo ($context["lowstock_title"] ?? null);
        echo "</h1>
    </div>
  </div>
    <div class=\"container-fluid\">
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"> ";
        // line 13
        echo ($context["text_lowstock"] ?? null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">
                <div class=\"row\">
            <div class=\"col-sm-8\">
            <table class=\"table table-bordered table-striped\">
              <thead>
                <tr>
                    <td class=\"text-left\">Prodect Name </td>
                    <td class=\"text-left\">Quantity of Product </td>
                </tr>
              </thead>
              <tbody>
              ";
        // line 26
        if (($context["stocks"] ?? null)) {
            // line 27
            echo "              ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["stocks"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["stock"]) {
                // line 28
                echo "                <tr>
                    <td class=\"text-left\">";
                // line 29
                echo twig_get_attribute($this->env, $this->source, $context["stock"], "name", [], "any", false, false, false, 29);
                echo "</td>
                    <td class=\"text-left\">";
                // line 30
                echo twig_get_attribute($this->env, $this->source, $context["stock"], "quantity", [], "any", false, false, false, 30);
                echo "</td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "              ";
        }
        // line 34
        echo "              </tbody>
            </table>
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
";
        // line 42
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "report/stock.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 42,  103 => 34,  100 => 33,  91 => 30,  87 => 29,  84 => 28,  79 => 27,  77 => 26,  61 => 13,  52 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "report/stock.twig", "");
    }
}
