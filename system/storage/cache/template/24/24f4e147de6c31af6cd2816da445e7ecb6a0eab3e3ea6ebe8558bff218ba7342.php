<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* report/lowstock.twig */
class __TwigTemplate_7ae9fe122663ab2f687bc0f6aea5e9db267af95c27d91a2688f560ee8289fec4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div id=\"content1\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1 style=\"color:DodgerBlue;\">";
        // line 4
        echo ($context["category_title"] ?? null);
        echo "</h1>
    </div>
    <div class=\"container-fluid\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <h3 class=\"panel-title\"> ";
        // line 9
        echo ($context["text_category"] ?? null);
        echo "</h3>
        </div>
      <div class=\"panel-body\">
        <div class=\"container\">
            <div class=\"container\">
            <h3> <b>Total Category:  ";
        // line 14
        echo ($context["total_category"] ?? null);
        echo " </b></h3> <br>
            <div class=\"row\">
            <div class=\"col-sm-6\">
            <table class=\"table table-bordered table-striped\">
              <thead>
                <tr>
                    <td class=\"text-left\">Category Name </td>
                    <td class=\"text-left\">No. of Product </td>
                </tr>
              </thead>
              <tbody>
              ";
        // line 25
        if (($context["categorys"] ?? null)) {
            // line 26
            echo "              ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categorys"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 27
                echo "                <tr>
                    <td class=\"text-left\">";
                // line 28
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 28);
                echo "</td>
                    <td class=\"text-left\">";
                // line 29
                echo twig_get_attribute($this->env, $this->source, $context["category"], "product_in_category", [], "any", false, false, false, 29);
                echo "</td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "              ";
        }
        // line 33
        echo "              </tbody>
            </table>
            </div>
            </div>
            <hr>
            
            


            <h4> Enable Category: <b> ";
        // line 42
        echo ($context["enable_category"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Disable Category: <b> ";
        // line 43
        echo ($context["disable_category"] ?? null);
        echo " </b></h4> <hr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>";
    }

    public function getTemplateName()
    {
        return "report/lowstock.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 43,  109 => 42,  98 => 33,  95 => 32,  86 => 29,  82 => 28,  79 => 27,  74 => 26,  72 => 25,  58 => 14,  50 => 9,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "report/lowstock.twig", "");
    }
}
