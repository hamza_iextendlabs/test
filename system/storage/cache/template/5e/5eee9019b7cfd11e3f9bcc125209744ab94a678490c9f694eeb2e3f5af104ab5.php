<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/test/test.twig */
class __TwigTemplate_eb7171b948ce14120155f0654b3384f07a2a221da5af30169721fee79bbc7a19 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
";
        // line 2
        if ((($context["marks"] ?? null) >= 90)) {
            // line 3
            echo " Your grade in A+
";
        } elseif ((        // line 4
($context["marks"] ?? null) >= 80)) {
            // line 5
            echo "Your grade in A
";
        } elseif ((        // line 6
($context["marks"] ?? null) >= 70)) {
            // line 7
            echo "Your grade in B
";
        } elseif ((        // line 8
($context["marks"] ?? null) >= 60)) {
            // line 9
            echo "Your grade in C
";
        } elseif ((        // line 10
($context["marks"] ?? null) >= 50)) {
            // line 11
            echo "Your grade in D
";
        } elseif ((        // line 12
($context["marks"] ?? null) > 40)) {
            // line 13
            echo "Your grade in F
";
        } elseif ((        // line 14
($context["marks"] ?? null) < 40)) {
            // line 15
            echo "Your grade in F
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/test/test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 15,  70 => 14,  67 => 13,  65 => 12,  62 => 11,  60 => 10,  57 => 9,  55 => 8,  52 => 7,  50 => 6,  47 => 5,  45 => 4,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/test/test.twig", "");
    }
}
