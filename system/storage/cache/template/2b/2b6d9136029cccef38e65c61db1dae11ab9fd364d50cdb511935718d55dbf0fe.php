<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* report/order.twig */
class __TwigTemplate_f5b2b793a53e9369e66f4389a2479665bc36b3e02ac9e6e4c8ef055288f1bbab extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <a href=\"";
        // line 6
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_back"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1 style=\"color:DodgerBlue;\">";
        // line 7
        echo ($context["order_title"] ?? null);
        echo "</h1>
    </div>
  </div>
    <div class=\"container-fluid\">
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"> ";
        // line 13
        echo ($context["text_order"] ?? null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">
                <div class=\"row\">
            <div class=\"col-sm-12\">
            <table class=\"table table-bordered table-striped\">
              <thead>
                <tr>
                    <td class=\"text-left\">Date </td>
                    <td class=\"text-left\">Order Id </td>
                    <td class=\"text-left\">Prodect Name </td>
                    <td class=\"text-left\">Quantity </td>
                    <td class=\"text-left\">Price of Prodect </td>
                    <td class=\"text-left\">Tax </td>
                    <td class=\"text-left\">Total Price </td>
                </tr>
              </thead>
              <tbody>
              ";
        // line 31
        if (($context["orders"] ?? null)) {
            // line 32
            echo "              ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["orders"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
                // line 33
                echo "                <tr>
                    <td class=\"text-left\">";
                // line 34
                echo twig_get_attribute($this->env, $this->source, $context["order"], "date_added", [], "any", false, false, false, 34);
                echo "</td>
                    <td class=\"text-left\">";
                // line 35
                echo twig_get_attribute($this->env, $this->source, $context["order"], "order_id", [], "any", false, false, false, 35);
                echo "</td>
                    <td class=\"text-left\">";
                // line 36
                echo twig_get_attribute($this->env, $this->source, $context["order"], "name", [], "any", false, false, false, 36);
                echo "</td>
                    <td class=\"text-left\">";
                // line 37
                echo twig_get_attribute($this->env, $this->source, $context["order"], "quantity", [], "any", false, false, false, 37);
                echo "</td>
                    <td class=\"text-left\">";
                // line 38
                echo twig_get_attribute($this->env, $this->source, $context["order"], "price", [], "any", false, false, false, 38);
                echo "</td>
                    <td class=\"text-left\">";
                // line 39
                echo twig_get_attribute($this->env, $this->source, $context["order"], "tax", [], "any", false, false, false, 39);
                echo "</td>
                    <td class=\"text-left\">";
                // line 40
                echo twig_get_attribute($this->env, $this->source, $context["order"], "total", [], "any", false, false, false, 40);
                echo "</td>
                </tr>
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "              ";
        } else {
            // line 44
            echo "                <h4 style=\"color:Red;\"> Your Product is Out Of Stock </h4>
              ";
        }
        // line 46
        echo "              </tbody>
            </table>
            
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
";
        // line 55
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "report/order.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 55,  132 => 46,  128 => 44,  125 => 43,  116 => 40,  112 => 39,  108 => 38,  104 => 37,  100 => 36,  96 => 35,  92 => 34,  89 => 33,  84 => 32,  82 => 31,  61 => 13,  52 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "report/order.twig", "");
    }
}
