<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/information/allnews.twig */
class __TwigTemplate_a3accc39e56dde69cc6885d3b6f8fa3656331f0b30686a62fbe64aecd0fab045 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"news-news\" class=\"container\">
  
  <div class=\"row\">";
        // line 4
        echo ($context["column_left"] ?? null);
        echo "
    ";
        // line 5
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 6
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 7
            echo "    ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 8
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 9
            echo "    ";
        } else {
            // line 10
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 11
            echo "    ";
        }
        // line 12
        echo "    <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo " \"row\">";
        echo ($context["content_top"] ?? null);
        echo "
        <div class =\"col-sm-12\">
      <h1> <u>";
        // line 14
        echo ($context["all_news"] ?? null);
        echo "</u> </h1>
      ";
        // line 15
        $context["no"] = 1;
        // line 16
        echo "      <ul class=\"list-unstyled\">
         ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["allNews"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["news"]) {
            // line 18
            echo "          <li> <h4><b>";
            echo ($context["no"] ?? null);
            echo "):-</b> <a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["news"], "href", [], "any", false, false, false, 18);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["news"], "title", [], "any", false, false, false, 18);
            echo "</a></h4></li>
          ";
            // line 19
            echo twig_get_attribute($this->env, $this->source, $context["news"], "date_time", [], "any", false, false, false, 19);
            echo "
          <hr>
          ";
            // line 21
            $context["no"] = (($context["no"] ?? null) + 1);
            // line 22
            echo "          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['news'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "        </ul>
        <h3 class=\"pull-right\"> ";
        // line 24
        echo ($context["total_news"] ?? null);
        echo (($context["no"] ?? null) - 1);
        echo " </h3>
        </div>
      ";
        // line 26
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 27
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
";
        // line 29
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/information/allnews.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 29,  123 => 27,  119 => 26,  113 => 24,  110 => 23,  104 => 22,  102 => 21,  97 => 19,  88 => 18,  84 => 17,  81 => 16,  79 => 15,  75 => 14,  67 => 12,  64 => 11,  61 => 10,  58 => 9,  55 => 8,  52 => 7,  49 => 6,  47 => 5,  43 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/information/allnews.twig", "");
    }
}
