<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/test/test.twig */
class __TwigTemplate_7fb09855ce0968cd1567a80ade0f915c69aca57ed50cfa1b6b36f4be3ac2c7ee extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
";
        // line 2
        if (($context["test"] ?? null)) {
            // line 3
            echo ($context["text_enabled"] ?? null);
            echo "
";
        } else {
            // line 5
            echo ($context["text_disabled"] ?? null);
            echo ($context["test"] ?? null);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/test/test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 5,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/test/test.twig", "");
    }
}
