<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* report/return.twig */
class __TwigTemplate_55f8d0feadda5d018d0bfddc0fdac7bd0aa2719ed61f55e30136fbbc85f76048 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <a href=\"";
        // line 6
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_back"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1 style=\"color:DodgerBlue;\">";
        // line 7
        echo ($context["return_title"] ?? null);
        echo "</h1>
    </div>
  </div>
  <div class=\"container-fluid\">
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"> ";
        // line 13
        echo ($context["text_return"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <div class=\"container\">
            <div class=\"container col-sm-10\">
            <h4> Total Return Product: <b> ";
        // line 18
        echo ($context["total_return_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Pending Return Product: <b> ";
        // line 19
        echo ($context["pending_return_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Complete Return Product: <b> ";
        // line 20
        echo ($context["complete_return_product"] ?? null);
        echo " </b></h4> <br><hr>
            <h4> Awaiting Return Product: <b> ";
        // line 21
        echo ($context["awaiting_return_product"] ?? null);
        echo " </b></h4> <hr>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
";
        // line 28
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "report/return.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 28,  81 => 21,  77 => 20,  73 => 19,  69 => 18,  61 => 13,  52 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "report/return.twig", "");
    }
}
