<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/test/test.twig */
class __TwigTemplate_d81435b9248687fb705a0e96a91e7f83d272c28ee20a4597b6a304bff5ee8ea3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        echo ($context["entry_status"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"test_status\" id=\"input-status\" class=\"form-control\">
                <option value=\"1\" selected=\"selected\">";
        // line 4
        echo ($context["text_enabled"] ?? null);
        echo "</option>
                <option value=\"0\">";
        // line 5
        echo ($context["text_disabled"] ?? null);
        echo "</option>
              </select>
              ";
        // line 7
        if (($context["test_status"] ?? null)) {
            // line 8
            echo "                Hallo
                ";
        } else {
            // line 10
            echo "                hallo
              ";
        }
    }

    public function getTemplateName()
    {
        return "default/template/test/test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 10,  55 => 8,  53 => 7,  48 => 5,  44 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/test/test.twig", "");
    }
}
