<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/test/test.twig */
class __TwigTemplate_e50a9685378ca8daf168ecf925fea60c084351b40825794e1e918b8850c32c0a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
              ";
        // line 2
        if (($context["seller_status"] ?? null)) {
            // line 3
            echo "                Status: Enable
                ";
        } else {
            // line 5
            echo "                Status: Disable
              ";
        }
        // line 7
        echo "                <br>


              ";
        // line 10
        if (($context["study"] ?? null)) {
            // line 11
            echo "                You have learned php.
                ";
        } else {
            // line 13
            echo "                You have not learned php.
              ";
        }
        // line 15
        echo "                <br>


              ";
        // line 18
        if ((($context["course"] ?? null) == "PHP")) {
            // line 19
            echo "                You select the ";
            echo ($context["course"] ?? null);
            echo " course.
                ";
            // line 20
            if ((($context["course"] ?? null) == "Javascript")) {
                // line 21
                echo "                You select the ";
                echo ($context["course"] ?? null);
                echo " course.
              ";
            }
            // line 23
            echo "              ";
        }
        // line 24
        echo "              
                
                <br>
              ";
        // line 27
        if (($context["msg"] ?? null)) {
            // line 28
            echo "                Message: ";
            echo ($context["msg"] ?? null);
            echo "
              ";
        }
        // line 30
        echo "              ";
    }

    public function getTemplateName()
    {
        return "default/template/test/test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 30,  95 => 28,  93 => 27,  88 => 24,  85 => 23,  79 => 21,  77 => 20,  72 => 19,  70 => 18,  65 => 15,  61 => 13,  57 => 11,  55 => 10,  50 => 7,  46 => 5,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/test/test.twig", "");
    }
}
