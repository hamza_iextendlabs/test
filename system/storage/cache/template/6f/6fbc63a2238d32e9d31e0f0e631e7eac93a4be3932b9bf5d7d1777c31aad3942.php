<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/test/test.twig */
class __TwigTemplate_e9e935c2d0c7507257e97ef5830048fd0f93ea15449e13a48569ec55496417f7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<form action=\"post\">
<label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 2
        echo ($context["entry_status"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"test_status\" id=\"input-status\" class=\"form-control\">
                ";
        // line 5
        if (($context["test_status"] ?? null)) {
            // line 6
            echo "                <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\">";
            // line 7
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        } else {
            // line 9
            echo "                <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 10
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        }
        // line 12
        echo "              </select>
            </div>
            </form>
";
        // line 15
        if (($context["test_status"] ?? null)) {
            // line 16
            echo ($context["test_status"] ?? null);
            echo "
";
        } else {
            // line 18
            echo ($context["test_status"] ?? null);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/test/test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  73 => 15,  68 => 12,  63 => 10,  58 => 9,  53 => 7,  48 => 6,  46 => 5,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/test/test.twig", "");
    }
}
