<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/information/news.twig */
class __TwigTemplate_12788eb83bcbd3262313b927d004a9efc7eae8ee885acf534ddaaabec4c012d3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"news-news\" class=\"container\">
  
  <div class=\"row\">";
        // line 4
        echo ($context["column_left"] ?? null);
        echo "
    ";
        // line 5
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 6
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 7
            echo "    ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 8
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 9
            echo "    ";
        } else {
            // line 10
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 11
            echo "    ";
        }
        // line 12
        echo "    <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "
      <h1> ";
        // line 13
        echo ($context["heading_title"] ?? null);
        echo " </h1>
      ";
        // line 14
        $context["no"] = 1;
        // line 15
        echo "      <ul class=\"list-unstyled\">
         ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["allNews"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["news"]) {
            // line 17
            echo "          <li> <b>";
            echo ($context["no"] ?? null);
            echo "):-</b> <h3><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["news"], "href", [], "any", false, false, false, 17);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["news"], "title", [], "any", false, false, false, 17);
            echo "</a></h3></li>
          ";
            // line 18
            echo twig_get_attribute($this->env, $this->source, $context["news"], "date_time", [], "any", false, false, false, 18);
            echo "
          <hr>
          ";
            // line 20
            $context["no"] = (($context["no"] ?? null) + 1);
            // line 21
            echo "          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['news'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "        </ul>
        <h4 class=\"pull-right\"> ";
        // line 23
        echo ($context["total"] ?? null);
        echo (($context["no"] ?? null) - 1);
        echo " </h4>
      ";
        // line 24
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 25
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
";
        // line 27
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/information/news.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 27,  121 => 25,  117 => 24,  112 => 23,  109 => 22,  103 => 21,  101 => 20,  96 => 18,  87 => 17,  83 => 16,  80 => 15,  78 => 14,  74 => 13,  67 => 12,  64 => 11,  61 => 10,  58 => 9,  55 => 8,  52 => 7,  49 => 6,  47 => 5,  43 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/information/news.twig", "");
    }
}
