<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/information/news.twig */
class __TwigTemplate_5427e926e979be4c20f05e68efe687a977c7bd03d991c9ff7e43dfb7de1c6111 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"product-category\" class=\"container\">
    <div id=\"content\" class=\"";
        // line 3
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "
      <h2>";
        // line 4
        echo ($context["heading_title"] ?? null);
        echo "</h2>
      <li>";
        // line 5
        echo ($context["text_title"] ?? null);
        echo " ";
        echo ($context["title"] ?? null);
        echo "</li>
      <li>";
        // line 6
        echo ($context["text_description"] ?? null);
        echo " ";
        echo ($context["description"] ?? null);
        echo "</li>
    </div>
</div>
";
        // line 9
        echo ($context["footer"] ?? null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "default/template/information/news.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 9,  58 => 6,  52 => 5,  48 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/information/news.twig", "");
    }
}
