<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* report/lowstock.twig */
class __TwigTemplate_917a7365f5cf0be46c93a7c7d9066e8694e7ec6d49041bfcfb21b0e74c8817fa extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <a href=\"";
        // line 6
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_back"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
                <h1 style=\"color:DodgerBlue;\">";
        // line 7
        echo ($context["lowstock_title"] ?? null);
        echo "</h1>
            </div>
        </div>
    </div>
</div>
<h3> <b>Low Stock Product </b></h3> <br>
            <div class=\"row\">
            <div class=\"col-sm-6\">
            <table class=\"table table-bordered table-striped\">
              <thead>
                <tr>
                    <td class=\"text-left\">Prodect Name </td>
                    <td class=\"text-left\">Quantity of Product </td>
                </tr>
              </thead>
              <tbody>
              ";
        // line 23
        if (($context["lowstocks"] ?? null)) {
            // line 24
            echo "              ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["lowstocks"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["lowstock"]) {
                // line 25
                echo "                <tr>
                    <td class=\"text-left\">";
                // line 26
                echo twig_get_attribute($this->env, $this->source, $context["lowstock"], "name", [], "any", false, false, false, 26);
                echo "</td>
                    <td class=\"text-left\">";
                // line 27
                echo twig_get_attribute($this->env, $this->source, $context["lowstock"], "quantity", [], "any", false, false, false, 27);
                echo "</td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lowstock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "              ";
        }
        // line 31
        echo "              </tbody>
            </table>
            </div>
            </div>
            <hr>
";
        // line 36
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "report/lowstock.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 36,  97 => 31,  94 => 30,  85 => 27,  81 => 26,  78 => 25,  73 => 24,  71 => 23,  52 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "report/lowstock.twig", "");
    }
}
