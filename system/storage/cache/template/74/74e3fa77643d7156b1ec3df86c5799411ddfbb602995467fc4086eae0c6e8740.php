<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* report/stock.twig */
class __TwigTemplate_10983772b88e30dbd9be7e4cc1c242ed0ecaa7a7b0407f7c1224b13f70fa34d6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <a href=\"";
        // line 6
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_back"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1 style=\"color:DodgerBlue;\">";
        // line 7
        echo ($context["lowstock_title"] ?? null);
        echo "</h1>
    </div>
  </div>
    <div class=\"container-fluid\">
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"> ";
        // line 13
        echo ($context["text_lowstock"] ?? null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">
                <div class=\"row\">
            <div class=\"col-sm-12\">
              ";
        // line 18
        if (($context["stocks"] ?? null)) {
            // line 19
            echo "            <table class=\"table table-bordered table-striped\">
              <thead>
                <tr>
                    <td class=\"text-left\">Prodect Name </td>
                    <td class=\"text-left\">Quantity of Product </td>
                </tr>
              </thead>
              <tbody>
              ";
            // line 27
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["stocks"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["stock"]) {
                // line 28
                echo "                <tr>
                    <td class=\"text-left\">";
                // line 29
                echo twig_get_attribute($this->env, $this->source, $context["stock"], "name", [], "any", false, false, false, 29);
                echo "</td>
                    <td class=\"text-left\">";
                // line 30
                echo twig_get_attribute($this->env, $this->source, $context["stock"], "quantity", [], "any", false, false, false, 30);
                echo "</td>
                </tr>
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "              </tbody>
            </table>
              ";
        } else {
            // line 36
            echo "              <h4 style=\"color:Red;\"> There is no Product in your selected range. </h4>
              ";
        }
        // line 38
        echo "            </div>
            </div>
            </div>
        </div>
    </div>
</div>
";
        // line 44
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "report/stock.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 44,  110 => 38,  106 => 36,  101 => 33,  92 => 30,  88 => 29,  85 => 28,  81 => 27,  71 => 19,  69 => 18,  61 => 13,  52 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "report/stock.twig", "");
    }
}
