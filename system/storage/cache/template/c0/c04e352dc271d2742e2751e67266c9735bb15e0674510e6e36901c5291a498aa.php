<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/test/test.twig */
class __TwigTemplate_fed195c3511aec9eada5c2356c3ef541b5c9eb4e1701cf0aa27f0a636691224d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "           <form>
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 2
        echo ($context["entry_status"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"test_status\" id=\"input-status\" class=\"form-control\">
                <option value=\"1\" selected=\"selected\">";
        // line 5
        echo ($context["text_enabled"] ?? null);
        echo "</option>
                <option value=\"0\">";
        // line 6
        echo ($context["text_disabled"] ?? null);
        echo "</option>
              </select>
              <input type=\"submit\" value=\"Submit\">
              </form>
              ";
        // line 10
        if (($context["test_status"] ?? null)) {
            // line 11
            echo "                Your are Enable
                ";
        } else {
            // line 13
            echo "                Your are Disable
              ";
        }
        // line 15
        echo "

            ";
        // line 17
        if ((($context["account_status_config"] ?? null) == "1")) {
            // line 18
            echo "                Account status is 'Enabled'
                ";
        } else {
            // line 20
            echo "                Account status is 'Disabled'
            ";
        }
        // line 22
        echo "

              ";
        // line 24
        echo ($context["account_status_config"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "default/template/test/test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 24,  81 => 22,  77 => 20,  73 => 18,  71 => 17,  67 => 15,  63 => 13,  59 => 11,  57 => 10,  50 => 6,  46 => 5,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/test/test.twig", "");
    }
}
