<?php
class ModelExtensionModuleReport extends Model {
    
  public function getTotalCategory() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category");

		return $query->row['total'];
	}

  public function getEnableCategory() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category where status = 1");

		return $query->row['total'];
	}

  public function getDisableCategory() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category where status = 0");

		return $query->row['total'];
	}

  public function getTotalCustomer(){
    $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer");

	  return $query->row['total'];
  }

  public function getCustomerLogin(){
      $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_login");

		  return $query->row['total'];
  }

  public function getTotalOrder(){
      $query = $this->db->query("SELECT COUNT(*) AS total  FROM " . DB_PREFIX . "order");

		  return $query->row['total'];
  }

  public function getOrderStatus(){
      $query = $this->db->query("SELECT os.order_status_id as order_status_id, os.name as name FROM oc_order_status os ORDER BY os.order_status_id ASC");
      
      return $query->rows;
  }

  public function getOrderStatusTotal($order_id){
      $query = $this->db->query("SELECT COUNT(os.order_status_id) AS total FROM oc_order_history oh JOIN oc_order_status os ON (oh.order_status_id = os.order_status_id) JOIN oc_order o ON (oh.order_id = o.order_id) WHERE os.order_status_id = '" . (int)$order_id . "' ");
		  return $query->row['total'];
  }

  public function getOrderTotalAmount($order_id){
    $query = $this->db->query("SELECT SUM(o.total) AS total_amount FROM oc_order_history oh JOIN oc_order_status os ON (oh.order_status_id = os.order_status_id) JOIN oc_order o ON (oh.order_id = o.order_id) WHERE os.order_status_id = '" . (int)$order_id . "' ");
    return $query->row['total_amount'];
  }

  public function getOrderStatusById($order_status_id){
    $query = $this->db->query("SELECT os.order_status_id as order_status_id, os.name as name FROM oc_order_status os  WHERE os.order_status_id='" . (int)$order_status_id . "' ");
    
    return $query->rows;
  }

  public function getOrderStatusTotalByDate($date_start,$date_end,$order_status_id){
    $query = $this->db->query("SELECT COUNT(os.order_status_id) AS total FROM oc_order o LEFT JOIN oc_order_history oh ON(o.order_id = oh.order_id) LEFT JOIN oc_order_status os ON(oh.order_status_id=os.order_status_id) WHERE os.order_status_id='" . (int)$order_status_id . "' AND oh.date_added BETWEEN '" . $this->db->escape($date_start) . "' AND '" . $this->db->escape($date_end) . "' ");
    return $query->row['total'];
  }

  public function getOrderTotalAmountByDate($date_start,$date_end,$order_status_id){
    $query = $this->db->query("SELECT SUM(o.total) AS total_amount FROM oc_order o LEFT JOIN oc_order_history oh ON(o.order_id = oh.order_id) LEFT JOIN oc_order_status os ON(oh.order_status_id=os.order_status_id) WHERE os.order_status_id='" . (int)$order_status_id . "' AND oh.date_added BETWEEN '" . $this->db->escape($date_start) . "' AND '" . $this->db->escape($date_end) . "' ");
    return $query->row['total_amount'];
  }

  public function getTotalOrderedProduct(){
      $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_product");

		  return $query->row['total'];
  }

  public function getTotalProduct(){
      $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product");

		  return $query->row['total'];
  }
    
  public function getTotalWishlistProduct(){
      $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_wishlist");

		  return $query->row['total'];
  }

  public function getTotalcartProduct(){
      $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cart");

  		return $query->row['total'];
  }

  public function getCategory(){
    $query = $this->db->query("SELECT name, category_id FROM " . DB_PREFIX . "category_description ORDER by category_id ASC");
    return $query->rows;
  }

  public function getTotalProductCategory($category_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_category where category_id = '" . (int)$category_id . "'");
		return $query->row['total'];
	}

  public function getLowStock($quantity =0){
    $query = $this->db->query("SELECT pd.name, p.quantity FROM " . DB_PREFIX . "product_description pd JOIN " . DB_PREFIX . "product p ON(pd.product_id = p.product_id) WHERE p.quantity <='" . (int)$quantity . "' ORDER by p.quantity ASC");
    return $query->rows;
  }

  public function getQuantityCategory($category_id,$quantity=0){
    $query = $this->db->query("SELECT pd.name, p.quantity FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON(p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category pc ON (p.product_id = pc.product_id) LEFT JOIN " . DB_PREFIX . "category_description cd ON(pc.category_id = cd.category_id) WHERE cd.category_id = '" . (int)$category_id . "' AND NOT p.quantity>'" . (int)$quantity . "'");
    return $query->rows;
  }

  public function getOrderDetail($date_start,$date_end){
    $query = $this->db->query("SELECT o.date_added,op.order_id,op.name,op.quantity,op.price,op.tax,o.total FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "order o ON(op.order_id = o.order_id) WHERE o.date_added BETWEEN  '" . $this->db->escape($date_start) . "' AND '" . $this->db->escape($date_end) . "' ");    
    return $query->rows;
  }

  public function getCompleteOrderDetail($date_start,$date_end){
    $query = $this->db->query("SELECT COUNT(os.order_status_id) AS total,SUM(o.total) AS total_amount FROM oc_order o LEFT JOIN oc_order_history oh ON(o.order_id = oh.order_id) LEFT JOIN oc_order_status os ON(oh.order_status_id=os.order_status_id) WHERE os.name='Complete' AND o.date_added BETWEEN '" . $this->db->escape($date_start) . "' AND '" . $this->db->escape($date_end) . "' ");    
    return $query->row;
  }

}