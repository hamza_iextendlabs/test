<?php
// Heading
$_['heading_title']    = 'Report';
$_['product_title']    = 'Product Report';
$_['category_title']   = 'Category Report';
$_['customer_title']   = 'Customer Report';
$_['lowstock_title']   = 'Low Stock Report';
$_['return_title']     = 'Return Report';
$_['order_title']      = 'Order Report';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified report module!';
$_['text_edit']        = 'Edit Report Module';
$_['text_product']     = 'Report Related to Product';
$_['text_category']    = 'Report Related to Category';
$_['text_quantity']    = 'Filter For Low Stock Product';
$_['text_ordered']     = 'Filter For Order';
$_['text_customer']    = 'Report Related to Customer';
$_['text_lowstock']    = 'Report Related to Lowstock Product';
$_['text_return']      = 'Report Related to Returned Product';
$_['text_order']       = 'Report Related to Complete Ordered';
$_['text_order_status']= 'Filter For Order by status';
$_['text_view']        = 'Some Report related to Store';

// Entry
$_['entry_quantity']    = 'Enter Quantity';
$_['entry_status']     = 'Status';
$_['select_category']   = 'Select Category';
$_['select_status']     = 'Select Order Status';
$_['date_start']        = 'Date Start';
$_['date_end']          = 'Date End';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify report module!';