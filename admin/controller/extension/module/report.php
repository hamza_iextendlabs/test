<?php
class ControllerExtensionModuleReport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/report');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_report', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/report', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/report', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_report_status'])) {
			$data['module_report_status'] = $this->request->post['module_report_status'];
		} else {
			$data['module_report_status'] = $this->config->get('module_report_status');
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->load->model('extension/module/report');
		

		$data['total_customer'] = $this->model_extension_module_report->getTotalCustomer();
		$data['login_customer'] = $this->model_extension_module_report->getCustomerLogin();
		$data['total_order'] = $this->model_extension_module_report->getTotalOrder();
		$data['total_category'] = $this->model_extension_module_report->getTotalCategory();
		$data['enable_category'] = $this->model_extension_module_report->getEnableCategory();
		$data['disable_category'] = $this->model_extension_module_report->getDisableCategory();
		$data['total_product'] = $this->model_extension_module_report->getTotalProduct();
		$data['total_ordered_product'] = $this->model_extension_module_report->getTotalOrderedProduct();
		$data['total_wishlist_product'] = $this->model_extension_module_report->getTotalWishlistProduct();
		$data['total_cart_product'] = $this->model_extension_module_report->getTotalcartProduct();
		
		$data['lowstocks'] = $this->model_extension_module_report->getLowStock();
		$data['user_token'] = $this->session->data['user_token'];
		$results= $this->model_extension_module_report->getCategory();
		$data['categorys'] = array();
		
		foreach ($results as $result) {
			$data['categorys'][] = array(
				'name'		 => $result['name'],
				'category_id'		 => $result['category_id'],
				'product_in_category' => $this->model_extension_module_report->getTotalProductCategory($result['category_id'])
				);
		}

		if (isset($this->request->get['date_start_filter'])) {
			$date_start_filter = $this->request->get['date_start_filter'];
		} else {
			$date_start_filter = '';
		}

		if (isset($this->request->get['date_end_filter'])) {
			$date_end_filter = $this->request->get['date_end_filter'];
		} else {
			$date_end_filter = '';
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = '';
		}



		$url = '';

		if (isset($this->request->get['date_start_filter'])) {
			$url .= '&date_start_filter=' . $this->request->get['date_start_filter'];
		}

		if (isset($this->request->get['date_end_filter'])) {
			$url .= '&date_end_filter=' . $this->request->get['date_end_filter'];
		}

		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		$data['order_statuss'] = $orders = $this->model_extension_module_report->getOrderStatus();

		$data['date_start_filter'] = $date_start_filter;
		$data['date_end_filter'] = $date_end_filter;
		$data['status'] = $status;


		if($data['date_start_filter'] || $data['date_end_filter']){ 
		
		$orders = $this->model_extension_module_report->getOrderStatusById($data['status']);
		$data['order_details'] = array();
		
		foreach ($orders as $order) {
			$data['order_details'][] = array(
				'name'		 				=> $order['name'],
				'total' 					=> $this->model_extension_module_report->getOrderStatusTotalByDate($data['date_start_filter'],$data['date_end_filter'],$data['status']),
				'total_amount' 				=> $this->model_extension_module_report->getOrderTotalAmountByDate($data['date_start_filter'],$data['date_end_filter'],$data['status'])
				);
		}

		}else{

		$orders = $this->model_extension_module_report->getOrderStatus();
		$data['order_details'] = array();
		
		foreach ($orders as $order) {
			$data['order_details'][] = array(
				'order_status_id'		 	=> $order['order_status_id'],
				'name'		 				=> $order['name'],
				'total' 					=> $this->model_extension_module_report->getOrderStatusTotal($order['order_status_id']),
				'total_amount' 					=> $this->model_extension_module_report->getOrderTotalAmount($order['order_status_id'])
				);
		}

		}

		$this->response->setOutput($this->load->view('extension/module/report', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/report')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	

	public function lowstock(){
		$this->load->language('extension/module/report');
		$this->load->model('extension/module/report');
		
		if (isset($this->request->get['quantity'])) {
			$quantity = $this->request->get['quantity'];
		} else {
			$quantity = '';
		}

		if (isset($this->request->get['category'])) {
			$category = $this->request->get['category'];
		} else {
			$category = '';
		}

		if (isset($this->request->get['date_start'])) {
			$date_start = $this->request->get['date_start'];
		} else {
			$date_start = '';
		}

		if (isset($this->request->get['date_end'])) {
			$date_end = $this->request->get['date_end'];
		} else {
			$date_end = '';
		}



		$url = '';

		if (isset($this->request->get['quantity'])) {
			$url .= '&quantity=' . $this->request->get['quantity'];
		}

		if (isset($this->request->get['category'])) {
			$url .= '&category=' . $this->request->get['category'];
		}

		if (isset($this->request->get['date_start'])) {
			$url .= '&date_start=' . $this->request->get['date_start'];
		}

		if (isset($this->request->get['date_end'])) {
			$url .= '&date_end=' . $this->request->get['date_end'];
		}


		$data['quantity'] = $quantity;
		$data['category'] = $category;
		$data['date_start'] = $date_start;
		$data['date_end'] = $date_end;
		

		$data['stocks'] = $this->model_extension_module_report->getQuantityCategory($data['category'],$data['quantity']);
		$data['orders'] = $this->model_extension_module_report->getOrderDetail($data['date_start'],$data['date_end']);


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['cancel'] = $this->url->link('extension/module/report', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);


		$this->response->setOutput($this->load->view('extension/module/stock', $data));
	}

	public function order(){
		$this->load->language('extension/module/report');
		$this->load->model('extension/module/report');
		

		if (isset($this->request->get['date_start'])) {
			$date_start = $this->request->get['date_start'];
		} else {
			$date_start = '';
		}

		if (isset($this->request->get['date_end'])) {
			$date_end = $this->request->get['date_end'];
		} else {
			$date_end = '';
		}



		$url = '';

		if (isset($this->request->get['date_start'])) {
			$url .= '&date_start=' . $this->request->get['date_start'];
		}

		if (isset($this->request->get['date_end'])) {
			$url .= '&date_end=' . $this->request->get['date_end'];
		}


		$data['date_start'] = $date_start;
		$data['date_end'] = $date_end;
		
		$results= $this->model_extension_module_report->getOrderDetail($data['date_start'],$data['date_end']);
		$data['orders'] = array();
		
		foreach ($results as $result) {
			$data['orders'][] = array(
				'date_added'		=> $result['date_added'],
				'order_id'		 	=> $result['order_id'],
				'name'		 		=> $result['name'],
				'quantity'		 	=> $result['quantity'],
				'price'		 		=> $result['price'],
				'tax'		 		=> $result['tax'],
				'total'		 		=> $result['total'],
				'view'          => $this->url->link('sale/order/info', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id'] . $url, true),
				);
		}
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['cancel'] = $this->url->link('extension/module/report', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);


		$this->response->setOutput($this->load->view('extension/module/order', $data));

	}
}